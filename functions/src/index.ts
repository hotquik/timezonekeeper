import * as functions from 'firebase-functions';
import * as firebase from 'firebase'
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import { usersRoutesConfig } from './users/routes-config';
import { timezonesRoutesConfig } from './timezones/routes-config';

// Firebase configuration (private):
var firebaseConfig = require("../firebase-config.json");
/*
var firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: ""
};
*/
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase adminsdk service account configuration (private):
var serviceAccount = require("../firebase-adminsdk-config.json");
/*
serviceAccount = {
  "type": "",
  "project_id": "",
  "private_key_id": "",
  "private_key": "",
  "client_email": "",
  "client_id": "",
  "auth_uri": "",
  "token_uri": "",
  "auth_provider_x509_cert_url": "",
  "client_x509_cert_url": ""
}
*/
// Initialize Firebase Admin
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://timezonekeeper.firebaseio.com"
});
 
const app = express();
app.use(bodyParser.json());
app.use(cors({ origin: true }));
// configure routes:
usersRoutesConfig(app)
timezonesRoutesConfig(app)

export const api = functions.https.onRequest(app);