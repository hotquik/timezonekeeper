import { Request } from 'express'

interface Filters {
    searchText: string,
    sortField: string, 
    sortOrder: string, 
    pageSize: number,
    pageNumber: number,
    pagesCount: number
}

interface paginateResult {
    records: Array<any>,
    filters: Filters
}

/*
Filters, sorts and paginates a record array based on request filters
*/
export function paginate ( req: Request, records: Array<any>, textFields: Array<string>) : paginateResult {
    let filters = {
        searchText: '',
        sortField: '', 
        sortOrder: '', 
        pageSize: 0,
        pageNumber: 1,
        pagesCount: 1
    }
    let result = records
    let searchText = req.query.searchText as string
    let sortField = req.query.sortField as string
    let sortOrder = req.query.sortOrder as string
    let pageSize = req.query.pageSize as string
    let pageNumber = req.query.pageNumber as string
    // filter records by searchText
    if (searchText) {
        filters.searchText = searchText
        result = result.filter(record => textFields.some(textField => record[textField].includes(searchText)))
    }
    // sort records by sortField and sortOrder
    if (sortField) {
        filters.sortField = sortField
        filters.sortOrder = sortOrder
        if (textFields.find(field => field === sortField)) {
            // sort as string
            result = result.sort((recordA, recordB) => (sortOrder === 'DESC' 
                ? ('' + recordB[sortField]).localeCompare(recordA[sortField]) 
                :  ('' + recordA[sortField]).localeCompare(recordB[sortField]) ))
        } else {
            // sort as number
            result = result.sort((recordA, recordB) => (sortOrder === 'DESC' 
                ? recordB[sortField] - recordA[sortField]
                : recordA[sortField] - recordB[sortField] ))
        }
    }
    // paginate by pageSize and pageNumber
    if (pageSize) {
        // default value for page number
        let pageNumberI = parseInt(pageNumber) || 1
        let pageSizeI = parseInt(pageSize)
        if (pageSizeI > 0) {
            let pagesCountI = Math.ceil(result.length / pageSizeI)
            // restrict pageNumberI values
            if (pageNumberI < 1) {
                pageNumberI = 1 
            } else if (pageNumberI > pagesCountI) {
                pageNumberI = pagesCountI
            } 
            filters.pageSize = pageSizeI
            filters.pageNumber = pageNumberI
            filters.pagesCount = pagesCountI
            const pageIndex = pageNumberI - 1
            const firstIndex = pageIndex * pageSizeI
            const lastIndex = (pageIndex + 1) * pageSizeI - 1
            result = result.filter((record, index) => firstIndex <= index && index <= lastIndex)
        }
    }
    return {
        records: result,
        filters
    }
}