import { Application } from 'express';
import { create, all, get, patch, remove } from './controller';
import { USER_ROLE, ADMIN_ROLE } from '../users/controller';
import { USERS_PATH }  from '../users/routes-config';
import { isAuthenticated } from '../auth/authenticated';
import { isAuthorized } from '../auth/authorized';

export const TIMEZONES_PATH = `${USERS_PATH}/:userId/timezones`

export function timezonesRoutesConfig(app: Application) {
    /*
    * AUTHENTICATED REQUESTS
    */
    // create a timezone
    app.post(`${TIMEZONES_PATH}`,[
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, USER_ROLE] }),
        create
    ]);
    // lists all timezones
    app.get(`${TIMEZONES_PATH}`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, USER_ROLE] }),
        all
    ]);
    // get :timezoneId timezone
    app.get(`${TIMEZONES_PATH}/:timezoneId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, USER_ROLE], allowSameUser: true }),
        get
    ]);
    // updates :timezoneId timezone
    app.patch(`${TIMEZONES_PATH}/:timezoneId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, USER_ROLE], allowSameUser: true }),
        patch
    ]); 
    // deletes :timezoneId timezone
    app.delete(`${TIMEZONES_PATH}/:timezoneId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, USER_ROLE] }),
        remove
    ]);
}