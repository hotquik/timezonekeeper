import { Request, Response } from 'express'
import * as admin from 'firebase-admin'
import { USER_ROLE, ADMIN_ROLE } from '../users/controller' 
import { paginate } from '../lib/common'

const TIMEZONES_COLLECTION: string = 'timezones'

// verify if the request user can CRUD timezones of a target user
function canCRUDtimezone (req: Request, res: Response, uid: string) {
    const authenticatedUserRole : string = res.locals.role
    if (authenticatedUserRole == ADMIN_ROLE) {
        // Admin can CRUD all time zones
        return true
    } else if (authenticatedUserRole == USER_ROLE) {
        // Regular users can only CRUD their own timezones
        const authenticatedUserUid : string = res.locals.uid
        return (authenticatedUserUid && uid && authenticatedUserUid === uid)
    } else {
        // No true condition matched. return false as default
        return false
    }
}

// validate a timezone offset
function validateOffset (offset: any) : boolean {
    return Number.isInteger(offset) && offset >= -12 * 60 && offset <= 14 * 60
}

// Handles a 'create a timezone resource' request
export async function create(req: Request, res: Response) {
    try {
        const { userId } = req.params
        const { name, city, offset } = req.body

        // Check all the required fields are received
        if (!userId || !name || !city || !validateOffset(offset)) {
            return res.status(400).send({ message: 'Missing fields' })
        }

        // Check if the authenticated user can CRUD timezones of the parameter user
        if (!canCRUDtimezone(req, res, userId)) {
            return res.status(403).send({ message: 'Forbidden' });
        }

        const db = admin.firestore()
        const timezoneId = db.collection(TIMEZONES_COLLECTION).doc().id;
        const userTimezonesRef = db.collection(TIMEZONES_COLLECTION).doc(userId)
        const data = { [timezoneId]: { name, city, offset } }
        await userTimezonesRef.set(data, {merge: true});
        // Succesfully created
        return res.status(201).send({ timezoneId })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'get all timezone resources' request
export async function all(req: Request, res: Response) {
    try {
        const { userId } = req.params
        // Check all the required fields are received
        if (!userId) {
            return res.status(400).send({ message: 'Missing fields' })
        }
        // Check if the authenticated user can CRUD timezones of the parameter user
        if (!canCRUDtimezone(req, res, userId)) {
            return res.status(403).send({ message: 'Forbidden' });
        }
        // Get list of timezones in Firestore
        const db = admin.firestore()
        const userTimezonesRef = db.collection(TIMEZONES_COLLECTION).doc(userId)
        const snapshot = await userTimezonesRef.get()
        const data = snapshot.data()
        const timezones = data ? Object.entries(data).map(([timezoneId, value]) => ({timezoneId, ...value})) : []
        // Paginate the list
        const list = paginate(req, timezones, ['name', 'city'])
        return res.status(200).send({ timezones: list.records, filters: list.filters })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'get a timezone resource' request
export async function get(req: Request, res: Response) {
    try {
        const { userId, timezoneId } = req.params
        // Check all the required fields are received
        if (!userId || !timezoneId) {
            return res.status(400).send({ message: 'Missing fields' })
        }
        // Check if the authenticated user can CRUD timezones of the parameter user
        if (!canCRUDtimezone(req, res, userId)) {
            return res.status(403).send({ message: 'Forbidden' });
        }
        // Get the specified timezone in Firestore
        const db = admin.firestore()
        const userTimezonesRef = db.collection(TIMEZONES_COLLECTION).doc(userId)
        const snapshot = await userTimezonesRef.get()
        const data = snapshot.data() 
        const timezone = data ? {timezoneId, ...data[timezoneId]} : {}
        return res.status(200).send({ timezone })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'patch a timezone resource' request
export async function patch(req: Request, res: Response) {
    try {        
        const { userId, timezoneId } = req.params
        const { name, city, offset } = req.body
        // Check all the required fields are received
        if (!userId || !timezoneId || !name || !city || !validateOffset(offset)) {
            return res.status(400).send({ message: 'Missing fields' })
        }
        // Check if the authenticated user can CRUD timezones of the parameter user
        if (!canCRUDtimezone(req, res, userId)) {
            return res.status(403).send({ message: 'Forbidden' });
        }
        // Get the specified timezone in Firestore
        const db = admin.firestore()
        const userTimezonesRef = db.collection(TIMEZONES_COLLECTION).doc(userId)
        const data = { [timezoneId]: { name, city, offset } }
        await userTimezonesRef.set(data, {merge: true});
        return res.status(200).send({ timezone: data })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'delete a timezone resource' request
export async function remove(req: Request, res: Response) {
    try {
        const { userId, timezoneId } = req.params
        // Check all the required fields are received
        if (!userId || !timezoneId) {
            return res.status(400).send({ message: 'Missing fields' })
        }
        // Check if the authenticated user can CRUD timezones of the parameter user
        if (!canCRUDtimezone(req, res, userId)) {
            return res.status(403).send({ message: 'Forbidden' });
        }
        // Get the specified timezone in Firestore
        const db = admin.firestore()
        const userTimezonesRef = db.collection(TIMEZONES_COLLECTION).doc(userId)
        await userTimezonesRef.update({ [timezoneId]: admin.firestore.FieldValue.delete() }); 
        return res.status(204).send({})
    } catch (err) {
        return handleError(res, err)
    }
}

function handleError(res: Response, err: any) {
    return res.status(500).send({ message: `${err.code} - ${err.message}` });
}