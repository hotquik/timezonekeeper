import { Request, Response } from 'express';

/*
Verifies the request user is authorized for the parameter options
*/
export function isAuthorized(opts: { hasRole: Array<'admin' | 'manager' | 'user'>, allowSameUser?: boolean }) {
    return (req: Request, res: Response, next: Function) => {
        const { role, uid } = res.locals
        const { userId } = req.params
        // User is authorized to CRUD its own records
        if (opts.allowSameUser && userId && uid === userId) {
            return next();
        }
        // Invalid role
        if (!role){
            return res.status(403).send({ message: 'Forbidden: Invalid role' });
        }
        // User role has permission for this request
        if (opts.hasRole.includes(role))
            return next();
        // User role has no permission for this request
        return res.status(403).send({ message: 'Forbidden: User role has no permission for this request' });
    }
}