import { Request, Response } from "express";
import * as admin from 'firebase-admin'

/*
Verifies and returns the idToken from the request. Returns undefined if no valid idToken is found
*/
export async function decodeIdToken (req: Request, res: Response) : Promise<admin.auth.DecodedIdToken | undefined>{
    const { authorization } = req.headers
    if (authorization && authorization.startsWith('Bearer')) {
        const split = authorization.split('Bearer ')
        if (split.length === 2) {
            const token = split[1]
            try {
                const decodedToken: admin.auth.DecodedIdToken = await admin.auth().verifyIdToken(token);
                // A valid token was found in the request
                res.locals = { ...res.locals, uid: decodedToken.uid, email: decodedToken.email, role: decodedToken.role }
                return decodedToken;
            }
            catch (err) {
                console.error(`${err.code} -  ${err.message}`)
                return undefined
            }         
        } else {
            return undefined
        }
    } else {
        return undefined
    }
}
/*
Verifies the request user has a valid idToken
*/
export async function isAuthenticated (req: Request, res: Response, next: Function) {
    const decodedToken = await decodeIdToken(req, res)
    if (decodedToken) {
        return next();
    } else {
        return res.status(401).send({ message: 'Unauthorized' });
    }
}