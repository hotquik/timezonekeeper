import { Request, Response } from 'express'
import * as admin from 'firebase-admin'
import * as firebase from 'firebase'
import { decodeIdToken } from '../auth/authenticated'
import { isAuthorized } from '../auth/authorized'
import { paginate } from '../lib/common'

export const USER_ROLE: 'user' | 'manager' | 'admin' = 'user'
export const MANAGER_ROLE: 'user' | 'manager' | 'admin' = 'manager'
export const ADMIN_ROLE: 'user' | 'manager' | 'admin' = 'admin'

const ROLE_PERMISSION_NONE : 'none' | 'self' | 'all' = 'none'
const ROLE_PERMISSION_SELF : 'none' | 'self' | 'all' = 'self'
const ROLE_PERMISSION_ALL : 'none' | 'self' | 'all' = 'all'

interface RolePermissionsRow {
    authenticatedUserRole: 'user' | 'manager' | 'admin',
    targetUserRole: 'user' | 'manager' | 'admin',
    permission: 'none' | 'self' | 'all' 
}

interface AppUser {
    userId: string,
    email: string,
    displayName: string,
    role: string,
    lastSignInTime: string,
    creationTime: string
}

const role_crud_permissions : Array<RolePermissionsRow> = [
    { authenticatedUserRole: USER_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_SELF }, 
    { authenticatedUserRole: USER_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: USER_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_ALL }, 
]


// verify if the request user can CRUD a target user based on both users roles
function canCRUDrole (req: Request, res: Response, targetRole: string, appUser?: AppUser) {
    const authenticatedUserRole : string = res.locals.role
    // Find the specific permission row in the table
    const permissionRow = role_crud_permissions.find(item => item.authenticatedUserRole === authenticatedUserRole && item.targetUserRole === targetRole)
    if (!permissionRow) {
        // If no row was found 
        return false
    } else {        
        if (permissionRow.permission === ROLE_PERMISSION_NONE) {
            // If permission is set to none, return false
            return false
        } else if (permissionRow.permission === ROLE_PERMISSION_ALL) {
            // If permission is set to all, return true       
            return true
        } else if (permissionRow.permission === ROLE_PERMISSION_SELF) {
            const authenticatedUserUid : string = res.locals.uid
            // If permission is set to self, check that the target user and the authenticated user are the same    
            return (authenticatedUserUid && appUser && appUser.userId && authenticatedUserUid === appUser.userId)
        } else {
            // No true condition matched. return false as default
            return false
        }
    }
}

// joins user data with custom claims
function mapUser(user: admin.auth.UserRecord) : AppUser {
    const customClaims = (user.customClaims || { role: '' }) as { role?: string }
    const role = customClaims.role ? customClaims.role : ''
    return {
        userId: user.uid,
        email: user.email || '',
        displayName: user.displayName || '',
        role,
        lastSignInTime: user.metadata.lastSignInTime,
        creationTime: user.metadata.creationTime
    }
}

// Internal. Create an user in the Firebase authentication
async function internalCreateUser (displayName: string, password: string, email: string, role: string) {
    // Create the user in Firebase
    const { uid } = await admin.auth().createUser({
        displayName,
        password,
        email
    })
    // Create the user role in custom claims
    await admin.auth().setCustomUserClaims(uid, { role })
    // Succesfully created
    return uid
}

// Handles a 'sign in' request
export async function signIn(req: Request, res: Response) {
    try {
        const { email, password } = req.body
        // Emulate an sign in Firebase as Auth has no method to do this
        const userCredential = await firebase.auth().signInWithEmailAndPassword(email, password);
        const idToken = await userCredential.user?.getIdToken();
        await firebase.auth().signOut()
        // Get user in Firestore
        const userRecord = await admin.auth().getUser(userCredential.user?.uid ? userCredential.user?.uid : '')
        const user = mapUser(userRecord)
        return res.status(200).send({ user, idToken })
    } catch (err) {
        return handleError(res, err)
    }
}

// Internal. Creates an user from a signup request
export async function signUp (req: Request, res: Response) {
    const { displayName, email, password } = req.body
    // Check all the required fields are received
    if (!displayName || !email || !password) {
        return res.status(400).send({ message: 'Missing fields' })
    }
    // Create the user in Firestore
    const uid = await internalCreateUser(displayName, password, email, USER_ROLE);
    // Succesfully created
    return res.status(201).send({ userId: uid })
}

// Internal. Creates first Admin. No other users must exist. Database must be empty.
async function createFirstAdmin(req: Request, res: Response) { 
    const db = admin.firestore()
    const { displayName, password, email } = req.body

    if (!displayName || !password || !email) {
        return res.status(400).send({ message: 'Missing fields' })
    }

    console.log("Install request received")
    // Verifies if any user is created
    const listUsersResult = await admin.auth().listUsers(1)
    if (listUsersResult.users.length > 0) {
        console.log("At least an user exist. Can't proceed with the install")
        return res.status(403).send({ message: 'Forbidden' })
    }

    // Verifies if any document exists in any collection
    let collections = await db.listCollections()
    let documentExists = collections.some(async (collection) => {
        const snapshot = await collection.limit(1).get();
        return snapshot.size > 0
    })
    if (documentExists) {
        console.log(`At least a document exists in the database. Can't proceed with the install`)
        return res.status(403).send({ message: 'Forbidden' });
    }
    
    // Proceed with the install
    // Create an Admin User in Firestore
    const uid = await internalCreateUser(displayName, password, email, ADMIN_ROLE);

    //await db.collection('')
    return res.status(201).send({ userId: uid })
}

// Internal. Creates an user
async function createUser(req: Request, res: Response) { 
    const { displayName, password, email, role } = req.body
    //console.log({ displayName, password, email, role });
    // Check all the required fields are received
    if (!displayName || !password || !email || !role) {
        return res.status(400).send({ message: 'Missing fields' })
    }
    // Check if the authenticated user can assign this role to the new user
    if (!canCRUDrole(req, res, role)) {
        return res.status(403).send({ message: 'Forbidden: User has no permission to assign this role' });
    }
    // Create the user in Firestore
    const uid = await internalCreateUser(displayName, password, email, role);
    // Succesfully created
    return res.status(201).send({ userId: uid })
}

// Handles a 'create an user resource' request
export async function create(req: Request, res: Response) {
    try {
        const decodedToken = await decodeIdToken(req, res) 
        if (decodedToken) {
            // User is authenticated. Request is 'Create User'
            return await isAuthorized({ hasRole: [ADMIN_ROLE, MANAGER_ROLE] })(req, res, () => createUser(req, res) )  
        } else {
            const { role } = req.body
            if (role === ADMIN_ROLE) {
                return await createFirstAdmin(req, res)
            } else if (role === USER_ROLE) {
                return await signUp(req, res)
            } else {
                return res.status(400).send({ message: 'Missing fields' })
            }
        }
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'get all user resources' request
export async function all(req: Request, res: Response) {
    try {
        // Get list of users in Firestore
        const listUsers = await admin.auth().listUsers()
        // Reformat the array for the response
        const users = listUsers.users.map(mapUser).filter(user => canCRUDrole(req, res, user.role))
        // Paginate the list
        const list = paginate(req, users, ['displayName', 'email', 'role'])
        return res.status(200).send({ users: list.records, filters: list.filters })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'get an user resource' request
export async function get(req: Request, res: Response) {
    try {
        const { userId } = req.params
        // Get user in Firestore
        const userRecord = await admin.auth().getUser(userId)
        const appUser = mapUser(userRecord)
        if (!canCRUDrole(req, res, appUser.role, appUser)) {
            return res.status(403).send({ message: 'Forbidden: User has no permission to assign this role' });
        }
        return res.status(200).send({ user: appUser })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'patch an user resource' request
export async function patch(req: Request, res: Response) {
   try {
        const { userId } = req.params
        const { displayName, password, email, role } = req.body;
        // Check all the required fields are received
        if (!userId || !displayName || !email || !role) {
            return res.status(400).send({ message: 'Missing fields' })
        }
        // Get user in Firestore
        let userRecord = await admin.auth().getUser(userId)
        let user = mapUser(userRecord)
        // Check if the authenticated user can assign this role to the user
        if (!canCRUDrole(req, res, role, user)) {
            return res.status(403).send({ message: 'Forbidden: User has no permission to assign this role' });
        }
        // If password is received , then update it
        let properties = (password ? { displayName, email, password } : { displayName, email })
        // Update the user in Firestore
        await admin.auth().updateUser(userId, properties);
        // Update the user role in custom claims
        await admin.auth().setCustomUserClaims(userId, { role });
        // Get het updated user in Firestore
        userRecord = await admin.auth().getUser(userId)
        user = mapUser(userRecord)

        return res.status(200).send({ user })
    } catch (err) {
        return handleError(res, err)
    }
}

// Handles a 'delete an user resource' request
export async function remove(req: Request, res: Response) {
   try {
       const { userId } = req.params
       // Get user in Firestore
       const userRecord = await admin.auth().getUser(userId)
       const user = mapUser(userRecord)
        // Check if the authenticated user can delete the user based on its role
        if (!canCRUDrole(req, res, user.role)) {
            return res.status(403).send({ message: 'Forbidden' });
        }
       await admin.auth().deleteUser(userId)
       return res.status(204).send({})
   } catch (err) {
       return handleError(res, err)
   }
}

function handleError(res: Response, err: any) {
    return res.status(500).send({ message: `${err.code} - ${err.message}` });
 }