import { Application } from 'express';
import { signIn, create, all, get, patch, remove , MANAGER_ROLE, ADMIN_ROLE } from './controller';
import { isAuthenticated } from '../auth/authenticated';
import { isAuthorized } from '../auth/authorized';

export const USERS_PATH = '/users'

export function usersRoutesConfig(app: Application) {
    /*
    * NON AUTHENTICATED REQUESTS
    */
    // create user
    app.post(`${USERS_PATH}`,
        create
    );
    // sign in user
    app.post(`${USERS_PATH}/auth`, 
        signIn
    );

    /*
    * AUTHENTICATED REQUESTS
    */
    // lists all users
    app.get(`${USERS_PATH}`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, MANAGER_ROLE] }),
        all
    ]);
    // get :userId user
    app.get(`${USERS_PATH}/:userId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, MANAGER_ROLE], allowSameUser: true }),
        get
    ]);
    // updates :userId user
    app.patch(`${USERS_PATH}/:userId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, MANAGER_ROLE], allowSameUser: true }),
        patch
    ]);
    // deletes :userId user
    app.delete(`${USERS_PATH}/:userId`, [
        isAuthenticated,
        isAuthorized({ hasRole: [ADMIN_ROLE, MANAGER_ROLE] }),
        remove
    ]);
}