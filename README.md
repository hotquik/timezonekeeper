# TimezoneKeeper
Live Demo: [http://timezonekeeper.demo.hotsoft.ninja/](http://timezonekeeper.demo.hotsoft.ninja/)
## functions
Backend: REST API. Created with NodeJS, ExpressJS and Firebase
## client-app
Frontend: Created with React, Bootstrap
## postman
Collection of REST API tests for Postman
