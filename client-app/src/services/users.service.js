import {
  _getJSON,
  _postJSON,
  _patchJSON,
  _delete,
  API_URL
} from '../lib/api.helper'
import { USER_ROLE } from '../lib/users.helper'

const USERS_URL = `${API_URL}/users`

/*
* NON AUTHENTICATED REQUESTS
*/
// sign up user
export const signUp = async (displayName, email, password) =>
  _postJSON(USERS_URL, '', {}, { displayName, email, password, role: USER_ROLE })
// login registered user
export const signIn = async (email, password) =>
  _postJSON(`${USERS_URL}/auth`, '', {}, { email, password })
  
/*
* AUTHENTICATED REQUESTS
*/
// create user
export const create = async (token, email, displayName, password, role) =>
  _postJSON(USERS_URL, token, {}, { email, displayName, password, role })
// lists all users
export const all = async (token, filters) =>
  _getJSON(USERS_URL, token, filters)
// get :userId user
export const get = async (token, userId) =>
  _getJSON(`${USERS_URL}/${userId}`, token, {})
// updates :userId user
export const patch = async (token, userId, email, displayName, password, role) =>
  _patchJSON(`${USERS_URL}/${userId}`, token, {}, { email, displayName, password, role })
// deletes :userId user
export const remove = async (token, userId) =>
  _delete(`${USERS_URL}/${userId}`, token, {})

