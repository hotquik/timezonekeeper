import {
  _getJSON,
  _postJSON,
  _patchJSON,
  _delete,
  API_URL
} from '../lib/api.helper'

const USERS_URL = `${API_URL}/users`
const timezonesUrl = (userId) => `${USERS_URL}/${userId}/timezones`

/*
* AUTHENTICATED REQUESTS
*/
// create timezone
export const create = async (token, userId, name, city, offset) =>
  _postJSON(timezonesUrl(userId), token, {}, { name, city, offset })
// lists all timezones
export const all = async (token, userId, filters) =>
  _getJSON(timezonesUrl(userId), token, filters)
// get :timezoneId timezone
export const get = async (token, userId, timezoneId) =>
  _getJSON(`${timezonesUrl(userId)}/${timezoneId}`, token, {})
// updates :timezoneId timezone
export const patch = async (token, userId, timezoneId, name, city, offset) =>
  _patchJSON(`${timezonesUrl(userId)}/${timezoneId}`, token, {}, { name, city, offset })
// deletes :timezoneId timezone
export const remove = async (token, userId, timezoneId) =>
  _delete(`${timezonesUrl(userId)}/${timezoneId}`, token, {})
