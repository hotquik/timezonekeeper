import React, { PureComponent } from 'react';
import './App.css';
import Layout from './components/Layout'

const LOCAL_STORAGE_USER_KEY = 'TIMEZONESKEEPER_LOCAL_STORAGE_USER_KEY'
const LOCAL_STORAGE_ID_TOKEN_KEY = 'TIMEZONESKEEPER_LOCAL_STORAGE_ID_TOKEN_KEY'

export default class App extends PureComponent {
  constructor (props) {
      super(props)
      // Try to retrieve auth from local storage
      let user, idToken
      try {
        user = JSON.parse(localStorage.getItem(LOCAL_STORAGE_USER_KEY))
        idToken = localStorage.getItem(LOCAL_STORAGE_ID_TOKEN_KEY)
      }
      catch { }
      if (user && idToken) {
        this.state = { user, idToken }
      } else {
        this.state = { user: null, idToken: null }
      }
  }
  
  /*
  Handles sign in , sign out of current user
  */
  onAuthChange = (user, idToken) => {
    if (user && idToken) {
      // Save auth to local storage
      localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(user) )
      localStorage.setItem(LOCAL_STORAGE_ID_TOKEN_KEY, idToken)
    } else {
      // Remove auth from local storage
      localStorage.removeItem(LOCAL_STORAGE_USER_KEY)
      localStorage.removeItem(LOCAL_STORAGE_ID_TOKEN_KEY)
    }
    // Set auth in app state
    this.setState({user, idToken})
  }

  render () {
    return <Layout {...this.props} {...this.state} onAuthChange={this.onAuthChange}/>
  }
}