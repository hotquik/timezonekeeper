import React from 'react'
import Container from 'react-bootstrap/Container'
import Jumbotron from 'react-bootstrap/Jumbotron'
/*
Home page component
*/
export default function Home (props) {
    return <Container>
        <Jumbotron>
            <h1>Welcome to TimezoneKeeper!</h1>
        </Jumbotron>
    </Container>
}