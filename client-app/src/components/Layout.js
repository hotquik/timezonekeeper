import React , { PureComponent } from 'react'
import AppNavbar from './AppNavbar';
import AppContent from './AppContent';

/*
Main layout component
*/
export default class Layout extends PureComponent {
    render () {
        return (
            <>
                <AppNavbar {...this.props} {...this.state} />
                <AppContent {...this.props} {...this.state} />
            </>
        )
    }
}