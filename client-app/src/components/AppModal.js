import { PureComponent } from 'react'

/*
Base class for this app modals
*/
export default class AppModal extends PureComponent {
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
            errors: []
        }
    }

    // Invokes a call to the api. Locks the modal while the call is being processed
    callApi = async (apiFunction) => {
        let errors = []
        // lock the modal
        this.setState({ loading: true, errors })
        // call the api
        apiFunction()
            .then(response => {
                // unlock the modal
                this.setState(state => ({ loading: false }))
            })
            .catch(apiFunctionException => {
                errors = [...errors, apiFunctionException]
                // unlock the modal, update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, ...errors] }))
            })
    }
}

