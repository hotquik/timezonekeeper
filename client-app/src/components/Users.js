import React, { PureComponent }  from 'react'
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import { defaultListFilters } from '../lib/api.helper'
import { all, remove } from '../services/users.service'
import { LoadingIcon } from './Loading'
import ResourcesTable from './ResourcesTable'
import ConfirmationModal from './ConfirmationModal'
import EditUserModal, { CREATE_MODE, PATCH_MODE } from './EditUserModal'

/*
Users page component
*/
export default class Users extends PureComponent {
    constructor (props) {
        super(props) 
        this.state = {
            users: [],
            filters: {...defaultListFilters},
            modalMode: null,
            showDeleteModal: false,
            targetUser: null,
            loading: false,
            errors: []
        }
    }

    // dismisses an error alert
    onErrorClose = (e, index) => {
        // Remove the error clicked from the state        
        this.setState(state => {
            let newState = { errors : state.errors.filter((error, eIndex) => index !== eIndex) }
                return newState
            })
    }

    // invokes the api to get all users
    getAllUsers = (filters) => {
        const { idToken } = this.props
        this.setState({ loading: true })

        all(idToken, filters)
            .then(response => {
                if (response.ok) {
                    this.setState({ users: response.json.users, filters: response.json.filters })
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                }
                this.setState({ loading: false })
            })
            .catch(apiFunctionException => {
                // update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, apiFunctionException] }))
            })
    }

    // Opens the user modal in create mode
    onCreateUserClick = (e) => {
        this.setState({ modalMode: CREATE_MODE })
    }

    // Opens the user modal in patch mode
    onPatchUserClick = (e, targetUser) => {
        this.setState({ modalMode: PATCH_MODE, targetUser })
    }

    // Clears user modal data. triggers an 'get all' api call if needed
    handleEditModalClose = (e, success = false) => { 
        this.setState({ modalMode: null, targetUser: null })
        if (success) {
            this.getAllUsers(this.state.filters)
        }
    }

    // Opens the user delete confirmation modal
    onDeleteUserClick = (e, targetUser) => {
        this.setState({ showDeleteModal: true, targetUser })
    }

    // Clears user delete confirmation modal data.
    handleDeleteModalClose = (e) => { 
        this.setState({ showDeleteModal: false, targetUser: null  })
    }

    // invokes the api to delete an user
    handleDeleteModalConfirm = (e) => {
        // Get parameters for the delete
        const { idToken } = this.props
        const { targetUser } = this.state
        // Close the modal, start the request
        this.setState({ showDeleteModal: false, targetUser: null, loading: true })
        // Call the api function
        remove(idToken, targetUser.userId)
            .then(response => {
                if (response.ok) {
                    this.getAllUsers(this.state.filters)
                } else {
                    // update the errors
                    this.setState(state => ({ loading: false, errors: [...state.errors, response.json.message] }))
                }
            })
            .catch(apiFunctionException => {
                // update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, apiFunctionException] }))
            })
    }

    render () {
        const { users, filters, modalMode, targetUser, showDeleteModal, loading, errors } = this.state

        const usersColumns = [ 
            { title: 'Display Name', key: 'displayName', sortable: true }, 
            { title: 'Email', key: 'email', sortable: true }, 
            { title: 'Role', key: 'role', sortable: true } 
        ]

        return <Container>
            <h1>Users { loading && <LoadingIcon /> }</h1>
            {errors.map((error, index) => 
                <Alert key={index} onClose={e => this.onErrorClose(e, index)} variant='danger' dismissible >{error.message ? error.message : error}</Alert>)}
            
            <ResourcesTable resourceName='user' resources={users} filters={filters} 
                columns={usersColumns} keyName={'userId'} 
                loading={loading}
                onCreateResourceClick={this.onCreateUserClick} onRefresh={this.getAllUsers}
                onPatchResourceClick={this.onPatchUserClick} onDeleteResourceClick={this.onDeleteUserClick}
                ></ResourcesTable>

            {modalMode && <EditUserModal {...this.props} mode={modalMode} targetUser={targetUser} handleClose={this.handleEditModalClose} ></EditUserModal>}
            {showDeleteModal && 
                <ConfirmationModal show={true} title='Delete user' message={`Are you sure you want to delete the user ${targetUser.email}`} 
                    confirmText='Delete user' handleClose={this.handleDeleteModalClose} handleConfirm={this.handleDeleteModalConfirm} ></ConfirmationModal>}
        </Container>
    }
}