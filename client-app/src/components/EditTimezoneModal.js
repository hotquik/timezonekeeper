import React from 'react'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import { splitOffset, joinOffset, timezonesOffsets } from '../lib/timezones.helper'
import { create, patch } from '../services/timezones.service'
import { LoadingIcon } from './Loading'
import ValidationIcon from './ValidationIcon'
import AppModal from './AppModal'

export const CREATE_MODE = 'create'
export const PATCH_MODE = 'patch'

/*
Modal for creating and editing a timezone
*/
export default class EditTimezoneModal extends AppModal {
    formClearState = {
        name: '',
        nameValid: false,
        city: '',
        cityValid: false,
        offset: 0,
        offsetValid: true
    }

    /*
    Props expected:
        mode : CREATE_MODE | PATCH_MODE
        handleClose
        targetTimezone?
        onSuccess?
    */
    constructor (props) {
        super(props) 

        const { targetTimezone } = props      
        if (targetTimezone) {
            let fields = ['name', 'city', 'offset']
            fields.forEach(fieldName => {
                this.state = { ...this.state, ...this.newFieldState(fieldName, targetTimezone[fieldName]) }
            })
        } else {
            this.state = {...this.state, ...this.formClearState}
        }
    }
    
    clearForm = () => {
        this.setState(this.formClearState)
    }

    // updates the form inputs changes to the state
    onControlChange = (e, name) => {
        let newState = this.newFieldState(name, e.target.value)        
        this.setState(newState)
    }
    
    // validates the form inputs and returns a state object
    newFieldState (name, value) {
        // update field state
        let newState = { [name]: (value === undefined || value === null ?  '' : value) }
        // update field validation
        const validKey = `${name}Valid`
        let validValue 
        if (name === 'name') {
            validValue = (value.length >= 1)
        } else if (name === 'city') {
            validValue = (value.length >= 1)
        } else if (name === 'offset') {
            validValue = true
        }
        if (validValue !== undefined) {
            newState[validKey] = validValue
        }
        return newState
    }    

    // updates the offset hours changes to the state
    onOffsetHoursChange = (e) => {
        // validate value
        let value = parseInt(e.target.value)
        if (value >= -12 && value <= 14) {
            this.setState(state => {
                let offset = state.offset
                let offsetObj = splitOffset(offset)
                offset = joinOffset({ ...offsetObj, sign: (value >= 0 ? 1 :-1), offsetHours: Math.abs(value) }) 
                return { offset }
            })
        }
    }

    // updates the offset minutes changes to the state
    onOffsetMinutesChange = (e) => {
        // validate value
        let value = parseInt(e.target.value)
        if (value >= 0 && value <= 59) {
            this.setState(state => {
                let offset = state.offset
                let offsetObj = splitOffset(offset)
                offset = joinOffset({ ...offsetObj, offsetMinutes: value }) 
                return { offset }
            })
        }
    }

    // loads the data of the selected default timezone to the form
    onTimezoneOffsetChange = (e) => {
        // load default values based on offset
        const offsetTimeOffset = timezonesOffsets.find(item => item.offset === parseInt(e.target.value))
        // validate and set each field
        let newState = {}
        Object.entries(offsetTimeOffset).forEach(([name, value]) => {
            newState = {...newState, ...this.newFieldState(name, value)}  
        })
        this.setState(newState)
    }

    // invokes the api to create a timezone
    onCreateClick = async (e) => {
        const { idToken, targetUser } = this.props
        const { name, city, offset} = this.state
        return this.callApi( 
            () => create(idToken, targetUser.userId, name, city, offset) 
            .then( response => {
                if (response.ok) {
                    const { handleClose, onSuccess } = this.props
                    // clear the form
                    this.clearForm()
                    // close the modal 
                    handleClose(e, true)
                    if (onSuccess) {
                        onSuccess(response)
                    }
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                } 
                return response
            })
        )
    }
    
    // invokes the api to patch a timezone
    onPatchClick = async (e) => {
        const { idToken, targetUser, targetTimezone } = this.props
        const { name, city, offset} = this.state
        return this.callApi( 
            () => patch(idToken, targetUser.userId, targetTimezone.timezoneId, name, city, offset) 
            .then( response => {
                if (response.ok) {
                    const { handleClose, onSuccess } = this.props
                    // clear the form
                    this.clearForm()
                    // close the modal 
                    handleClose(e, true)
                    if (onSuccess) {
                        onSuccess(response)
                    }
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                } 
                return response
            })
        )
    }
    
    render () {
        const { mode, handleClose } = this.props
        const { name, nameValid, city, cityValid, offset, offsetValid, loading, errors } = this.state

        const offsetObj = splitOffset(offset)

        let showModal = true, formIsValid, title, submitFunction, submitText
        switch(mode) {
            case CREATE_MODE:
                title = 'Create Timezone'
                formIsValid = nameValid && cityValid && offsetValid
                submitFunction = this.onCreateClick
                submitText = 'Create'
                break
            case PATCH_MODE:
                title = 'Edit Timezone'
                formIsValid = nameValid && cityValid && offsetValid
                submitFunction = this.onPatchClick
                submitText = 'Save'
                break
            default:
                showModal = false
        }

        return ( 
            <Modal backdrop={loading ? 'static' : true} show={showModal} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>{title}</Modal.Title>
                    {loading && <LoadingIcon/>}
                </Modal.Header>
                <Modal.Body>
                    {errors.map((error, index) => <Alert key={index} variant='danger'>{error.message ? error.message : error}</Alert>)}
                    <Form>
                                             
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <ValidationIcon valid={nameValid} />
                            <Form.Control value={name} onChange={e => this.onControlChange(e, 'name')} disabled={loading}
                                type="text" placeholder="Enter name" />
                        </Form.Group>                        

                        <Form.Group controlId="formBasicCity">
                            <Form.Label>City</Form.Label>
                            <ValidationIcon valid={cityValid} />
                            <Form.Control value={city} onChange={e => this.onControlChange(e, 'city')} disabled={loading} 
                                type="text" placeholder="Enter city" />
                        </Form.Group>

                        <Form.Group controlId="formBasicOffsetHours">
                            <Form.Label>Offset Hours</Form.Label>
                            <ValidationIcon valid={offsetValid} />
                            <Form.Control value={offsetObj.sign * offsetObj.offsetHours} onChange={e => this.onOffsetHoursChange(e, 'offsetHours')} disabled={loading} 
                                type="number" placeholder="Enter Offset hours" />
                        </Form.Group>

                        <Form.Group controlId="formBasicOffsetMinutes">
                            <Form.Label>Offset Minutes</Form.Label>
                            <ValidationIcon valid={offsetValid} />
                            <Form.Control value={offsetObj.offsetMinutes} onChange={e => this.onOffsetMinutesChange(e, 'offsetMinutes')} disabled={loading} 
                                type="number" placeholder="Enter Offset minutes" />
                        </Form.Group>

                        <Form.Group controlId="formBasicOffset">
                            <Form.Label>Copy from</Form.Label>
                            <Form.Control onChange={e => this.onTimezoneOffsetChange(e)} disabled={loading} as="select" custom >
                                { timezonesOffsets.map((timezone, index) => <option key={index} value={timezone.offset} >{timezone.name} {timezone.city}</option>) }                                    
                            </Form.Control>
                        </Form.Group>
                        
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleClose} disabled={loading} variant="secondary" >
                        Cancel
                    </Button>
                    <Button disabled={!formIsValid || loading} onClick={submitFunction} variant="primary" >
                        {submitText}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

