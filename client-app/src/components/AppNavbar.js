import React , { PureComponent } from 'react'
import { Link } from "react-router-dom"
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import { roleCanAccessRoute } from '../lib/users.helper'
import { routes } from '../lib/routes.helper'
import EditUserModal, { SIGN_IN_MODE, SIGN_UP_MODE, PATCH_MODE } from './EditUserModal'

/*
Main app nav bar
*/
export default class AppNavbar extends PureComponent {
    constructor (props) {
        super(props)
        this.state = { modalMode: null, modalTargetUser: null, onModalSuccess: null }
    }

    // Opens the user modal in sign in mode
    handleSignInShow = (e) => { 
        this.setState({ modalMode: SIGN_IN_MODE })
    }

    // Opens the user modal in sign up mode
    handleSignUpShow = (e) => { 
        this.setState({ modalMode: SIGN_UP_MODE })
    }

    // Opens the user modal in patch current user mode
    handleProfileShow = (e, modalTargetUser) => {
        this.setState({ modalMode: PATCH_MODE, modalTargetUser, onModalSuccess: this.onProfileEditSuccess })
    }

    // Updates the current user data in the App state
    onProfileEditSuccess = (response) => {
        const { idToken, onAuthChange } = this.props
        onAuthChange(response.json.user, idToken)
    }

    // Clears user modal data
    handleModalClose = (e) => { 
        this.setState({ modalMode: null, modalTargetUser: null, onModalSuccess: null })
    }

    // Signs out the current user
    onSignOutClick = (e) => { 
        this.props.onAuthChange(null, null)
    }

    render () {
        const { user } = this.props
        const { modalMode, modalTargetUser, onModalSuccess } = this.state
        const { timezones, users, home } = routes
        return (
            <>
                <Navbar className='nav-bar-bg' variant="dark" expand="lg">
                    <Link to={home.path} className="navbar-brand nav-bar-text">TimezoneKeeper</Link> 
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            {user 
                            && <>                           
                                {roleCanAccessRoute(user.role, timezones) 
                                && <Navbar.Text><Link to={timezones.path} className="nav-link nav-bar-text">{timezones.title}</Link></Navbar.Text>}
                                {roleCanAccessRoute(user.role, users) 
                                && <Navbar.Text><Link to={users.path} className="nav-link nav-bar-text">{users.title}</Link></Navbar.Text>}
                            </>}
                        </Nav>
                        <Nav>
                            { 
                                user 
                                ? 
                                    <NavDropdown title={user.displayName} id="basic-nav-dropdown" className=" nav-bar-text" alignRight>
                                        <NavDropdown.Item onClick={e => this.handleProfileShow(e, user)} >Profile</NavDropdown.Item>
                                        <NavDropdown.Item onClick={this.onSignOutClick} >Sign Out</NavDropdown.Item>
                                    </NavDropdown>                                   
                                :
                                <>
                                    <span className="nav-link nav-bar-text"><Nav.Link onClick={this.handleSignInShow} >Sign In</Nav.Link></span>
                                    <span className="nav-link nav-bar-text"><Nav.Link onClick={this.handleSignUpShow} >Sign Up</Nav.Link></span>
                                </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                {modalMode && <EditUserModal {...this.props} mode={modalMode} targetUser={modalTargetUser} 
                    onSuccess={onModalSuccess} handleClose={this.handleModalClose}></EditUserModal>}
            </>
        )
    }
}

