import React, { PureComponent } from 'react'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { MdWarning } from "react-icons/md";

/*
Basic modal for confirmation request
*/
export default class ConfirmationModal extends PureComponent {
    /*
    Props expected:
        show, title, message, confirmText, handleClose, handleConfirm
    */       
    render () {
        const { show, title, message, confirmText, handleClose, handleConfirm } = this.props

        return ( 
            <Modal show={show} onHide={handleClose}>
                <Modal.Header>                    
                    <Modal.Title>{title}<MdWarning className='text-warning validationIcon m-2'/></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {message}
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleClose} variant="secondary" >
                        Cancel
                    </Button>
                    <Button onClick={handleConfirm} variant="primary" >
                        {confirmText}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

