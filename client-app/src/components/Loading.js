import logo from '../logo.svg';
import React from 'react';

/*
Loading animation, icon size 
*/
export function LoadingIcon () {
    return <img src={logo} className="App-logo loadingIcon" alt="logo" />
}

/*
Loading animation, table row size 
*/
export function LoadingRow () {
    return <tr><td colSpan="100%"><div className="mx-auto loadingRow"><img src={logo} className="App-logo loadingRowIcon" alt="logo" /></div></td></tr>
}