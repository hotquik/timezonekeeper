import React, { PureComponent }  from 'react'
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import { defaultListFilters } from '../lib/api.helper'
import { all } from '../services/users.service'
import { LoadingIcon } from './Loading'
import ResourcesTable from './ResourcesTable'

/*
Component for selecting a target user
*/
export default class UserSelector extends PureComponent {
    constructor (props) {
        super(props) 
        this.state = {
            users: [],
            filters: {...defaultListFilters},
            loading: false,
            errors: []
        }
    }

    // dismisses an error alert
    onErrorClose = (e, index) => {
        // Remove the error clicked from the state        
        this.setState(state => {
            let newState = { errors : state.errors.filter((error, eIndex) => index !== eIndex) }
                return newState
            })
    }

    // invokes the api to get all users
    getAllUsers = (filters) => {
        const { idToken } = this.props
        this.setState({ loading: true })

        all(idToken, filters)
            .then(response => {
                if (response.ok) {
                    this.setState({ users: response.json.users, filters: response.json.filters })
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                }
                this.setState({ loading: false })
            })
            .catch(apiFunctionException => {
                // update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, apiFunctionException] }))
            })
    }

    render () {
        const { targetUser, onSelectUserClick } = this.props
        const { users, filters, loading, errors } = this.state

        const usersColumns = [ 
            { title: 'Display Name', key: 'displayName', sortable: true  } , 
            { title: 'Email', key: 'email', sortable: true  } 
        ]

        return (targetUser 
            ? <Card>
                <Row className="p-2">
                    <Col sm={4} md={4}  className="py-2 px-4">
                        <span>{targetUser.displayName}</span>
                    </Col>
                    <Col sm={4} md={4}  className="py-2 px-4">
                        <span>{targetUser.email}</span>
                    </Col>
                    <Col sm={4} md={4} >
                        <Button onClick={e => onSelectUserClick(e, null)} variant="primary" className='m-1'>Change User</Button>
                    </Col>
                </Row>
            </Card>
            : <>
            <h1>Select an user { loading && <LoadingIcon /> }</h1>
            {errors.map((error, index) => 
                <Alert key={index} onClose={e => this.onErrorClose(e, index)} variant='danger' dismissible >{error.message ? error.message : error}</Alert>)}
            
            <ResourcesTable resourceName='user' resources={users} filters={filters} 
                columns={usersColumns} keyName={'userId'} 
                loading={loading}
                onRefresh={this.getAllUsers} onSelectResourceClick={onSelectUserClick}                
                ></ResourcesTable>
        </>)
    }
}