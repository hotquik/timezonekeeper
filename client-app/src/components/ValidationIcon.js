import React from 'react'
import { MdWarning, MdDone } from "react-icons/md";

/*
Validation icon for form inputs
*/
export default function ValidationIcon (props) {
    return props.valid ? <MdDone className='text-success validationIcon m-2'/> : <MdWarning className='text-warning validationIcon m-2'/>
}