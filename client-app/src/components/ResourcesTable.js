import React, { PureComponent } from 'react'
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Pagination from 'react-bootstrap/Pagination';
import { MdRefresh, MdMoreVert, MdSearch, MdArrowUpward, MdArrowDownward } from 'react-icons/md';
import { defaultPageSizes } from '../lib/api.helper'
import { LoadingRow } from './Loading'

const additionalPages = 2
const searchTextDelay = 1000

/*
Component for displaying, searching and sorting a list of resources
*/
export default class ResourcesTable extends PureComponent {
    searchTextTimeout = null
    
    constructor (props) {
        super(props)
        // searchText is rendered from the state instead of the props, for instant feeback
        this.state = {
            searchText: props.filters.searchText
        }
    }
    
    // trigger a 'get all resources' api call
    componentDidMount() {
        const { filters, onRefresh } = this.props
        onRefresh(filters)
    }

    // handle search text changes, trigger a 'get all resources' api call. api calls are invoked after a small delay
    onSearchTextChange = (e) => {
        const { filters, onRefresh } = this.props
        const searchText = e.target.value 
        this.setState({ searchText })
        // Clear previous timeout if it exists
        if (this.searchTextTimeout) {
            clearTimeout(this.searchTextTimeout)
            this.searchTextTimeout = null
        }
        // Schedule a new timeout to invoke the api with a delay
        this.searchTextTimeout = setTimeout(
            () => {
                this.searchTextTimeout = null
                onRefresh({ ...filters, searchText })
            }, 
            searchTextDelay
        )
    }

    // update the searchText in the state when it changes in the props
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevProps.filters.searchText !== this.props.filters.searchText) {
            this.setState({searchText: this.props.filters.searchText})
        }
    }

    // trigger a 'get all resources' api call
    onRefreshClick = (e) => {
        const { filters, onRefresh } = this.props
        onRefresh(filters)
    }

    // updates the sortable filter, trigger a 'get all resources' api call
    onColumnHeaderClick = (e, field) => {
        const { filters, onRefresh } = this.props
        if (filters.sortField === field) {
            onRefresh({ ...filters, sortOrder: (filters.sortOrder === 'ASC' ? 'DESC' : 'ASC') })
        } else {
            onRefresh({ ...filters, sortField: field, sortOrder: 'ASC' })
        }
    }

    // updates the page number filter, trigger a 'get all resources' api call
    onPageNumberClick = (e, pageNumber) => {
        const { filters, onRefresh } = this.props
        onRefresh({ ...filters, pageNumber })
    }

    // updates the page size filter, trigger a 'get all resources' api call
    onPageSizeChange = (e) => {
        const { filters, onRefresh } = this.props
        //this.setState({ pageSize: e.target.value })
        onRefresh({ ...filters, pageSize: e.target.value })
    }

    render () {

        const { title, resourceName, resources, columns, keyName, loading, onCreateResourceClick,  
            onPatchResourceClick, onDeleteResourceClick, onSelectResourceClick, filters } = this.props
        let { /*searchText,*/ sortField, sortOrder, pageSize, pageNumber, pagesCount } = filters
        let { searchText } = this.state
        const showActionsColumn = onPatchResourceClick || onDeleteResourceClick
        const showActionsDropdown = onPatchResourceClick || onDeleteResourceClick
        // CREATE PAGINATION CONTROL:
        // default pagesCount 
        pagesCount = pagesCount || 1
        // default page pageNumber
        pageNumber = pageNumber || 1
        // default pageSize
        pageSize = pageSize || defaultPageSizes[0]
        // calculate first and last pages numbers to show
        let firstPage, lastPage
        if (pageNumber - additionalPages < 1) {
            firstPage = 1
            lastPage = Math.min(additionalPages * 2 + firstPage, pagesCount)
        } else if (pageNumber + additionalPages > pagesCount) {
            lastPage = pagesCount
            firstPage = Math.max(lastPage - additionalPages * 2, 1)
        } else {
            firstPage = pageNumber - additionalPages
            lastPage = pageNumber + additionalPages
        }
        // generate first and last pages items
        let paginationItems = [] 
        for (let i = firstPage; i <= lastPage; i++) {
            paginationItems.push(<Pagination.Item active={i === pageNumber} key={i} onClick={e => this.onPageNumberClick(e, i)}>{i}</Pagination.Item>)
        }
        // create pagination control
        let pagination = (pagesCount > 1 &&
            <Pagination>
                {pageNumber > 1 && <Pagination.First onClick={e => this.onPageNumberClick(e, 0)}/>}
                {pageNumber > 1 && <Pagination.Prev onClick={e => this.onPageNumberClick(e, pageNumber - 1)}/>}
                {firstPage > 1 && <Pagination.Ellipsis />}                            
                {paginationItems}
                {lastPage < pagesCount && <Pagination.Ellipsis />}
                {pageNumber < pagesCount && <Pagination.Next onClick={e => this.onPageNumberClick(e, pageNumber + 1)}/>}
                {pageNumber < pagesCount && <Pagination.Last onClick={e => this.onPageNumberClick(e, pagesCount)}/>}
            </Pagination>)

        const titles = <tr>
                {columns.map((column, index) => 
                    <th key={index} onClick={!loading && column.sortable ? e => this.onColumnHeaderClick(e, column.sortKey || column.key) : undefined} 
                        className={!loading && column.sortable ? 'clickable' : ''}>
                        {column.title}
                        {(column.sortKey === sortField || column.key === sortField) && (sortOrder === 'ASC' ? <MdArrowUpward /> : <MdArrowDownward />)}
                    </th>)}
                {showActionsColumn && <th></th>}
            </tr>

        return <Card>
                {title && <Row className="p-2"><h2>{title}</h2></Row>}
                <Row className="p-2">
                    <Col sm={12} md={8} lg={9}><MdSearch className='toolBarIcon m-2'/>
                        <Form.Control disabled={loading} value={searchText} onChange={this.onSearchTextChange} placeholder={`Search ${resourceName}s`} 
                            type="text" className='search-input'/>
                    </Col>
                    <Col sm={12} md={4} lg={3}>
                        {onCreateResourceClick && <Button disabled={loading} onClick={onCreateResourceClick} variant="primary" className='m-1'>Create {resourceName}</Button>}
                        <Button onClick={this.onRefreshClick} disabled={loading} variant="light" className='roundedButton'>
                                <MdRefresh className='toolBarIcon'/>
                        </Button>
                    </Col>
                </Row>

                <Table hover  responsive>
                    <thead>
                        {titles}
                    </thead>
                    <tbody>
                        {   resources.length === 0
                            ?   loading ? <LoadingRow/> : <tr><td colSpan="100%"></td></tr>
                            :   resources.map(resource => 
                                    <tr key={resource[keyName]} >
                                        {columns.map((column, index) => 
                                            <td key={index} onClick={onSelectResourceClick && !loading ? e => onSelectResourceClick(e, resource) : undefined} 
                                                className={onSelectResourceClick && !loading ? 'clickable' : ''}>
                                                {resource[column.key]}
                                            </td>)}
                                        {showActionsColumn && <td>
                                            {showActionsDropdown && <Dropdown>
                                                <Dropdown.Toggle variant="light" id="dropdown-basic" bsPrefix='roundedButton' >
                                                    <MdMoreVert className='toolBarIcon'/>
                                                </Dropdown.Toggle>
                                                
                                                <Dropdown.Menu alignRight>
                                                    {onPatchResourceClick && <Dropdown.Item disabled={loading} onClick={e => onPatchResourceClick(e, resource)} >Edit {resourceName}</Dropdown.Item>}
                                                    {onDeleteResourceClick && <Dropdown.Item disabled={loading} onClick={e => onDeleteResourceClick(e, resource)} >Delete {resourceName}</Dropdown.Item>}
                                                </Dropdown.Menu>
                                            </Dropdown>}
                                        </td>}
                                    </tr>)
                        }                        
                    </tbody>
                    <tfoot>
                        {titles}
                    </tfoot>

                </Table>

                <Row className="p-2">
                    <Col sm={6} md={3}>
                        Rows per page: 
                    </Col>
                    <Col sm={6} md={3}>
                        <Form.Control onChange={e => this.onPageSizeChange(e)} disabled={loading} value={pageSize} as="select" custom >
                            { defaultPageSizes.map((item, index) => <option key={index} value={item} >{item}</option>) }                                    
                        </Form.Control>
                    </Col>

                    <Col sm={12} md={6}>
                        {pagination}
                    </Col>
                </Row>
            </Card>
    }         
}