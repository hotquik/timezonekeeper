import React from 'react'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import { USER_ROLE, MANAGER_ROLE, ADMIN_ROLE, canCRUDrole } from '../lib/users.helper'
import { signIn, signUp, create, patch } from '../services/users.service'
import { LoadingIcon } from './Loading'
import ValidationIcon from './ValidationIcon'
import AppModal from './AppModal'

export const SIGN_IN_MODE = 'signIn'
export const SIGN_UP_MODE = 'signUp'
export const CREATE_MODE = 'create'
export const PATCH_MODE = 'patch'

/*
Modal for signing in, signing up, creating and editing a user
*/
export default class EditUserModal extends AppModal {
    formClearState = {
        displayName: '',
        displayNameValid: false,
        email: '',
        emailValid: false,
        password: '',
        passwordValid: false,
        role: USER_ROLE
    }

    /*
    Props expected:
        mode : SIGN_IN_MODE | SIGN_UP_MODE | CREATE_MODE | PATCH_MODE
        handleClose
        targetUser?
        onSuccess?
    */
    constructor (props) {
        super(props) 

        const { targetUser } = props      
        if (targetUser) {
            let fields = ['displayName', 'email', 'password', 'role']
            fields.forEach(fieldName => {
                this.state = { ...this.state, ...this.newFieldState(fieldName, targetUser[fieldName]) }
            })
        } else {
            this.state = {...this.state, ...this.formClearState}
        }
    }
    
    clearForm = () => {
        this.setState(this.formClearState)
    }

    // updates the form inputs changes to the state
    onControlChange = (e, name) => {
        let newState = this.newFieldState(name, e.target.value)        
        this.setState(newState)
    }

    // validates the form inputs and returns a state object
    newFieldState (name, value) {
        // update field state
        let newState = { [name]: (value ? value : '') }
        // update field validation
        const validKey = `${name}Valid`
        let validValue 
        if (name === 'displayName') {
            validValue = (value.length >= 1)
        } else if (name === 'email') {
            validValue = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)
        } else if (name === 'password') {            
            validValue = 
                // password can be empty if its an user update
                (this.props.mode === PATCH_MODE && (!value || value.length === 0)) 
                // password should be 6 chars long according to Firebase
                || (value.length >= 6) 
        }
        if (validValue !== undefined) {
            newState[validKey] = validValue
        }
        return newState
    }
    
    // invokes the api to sign in an user
    onSignInClick = async (e) => {
        const { email, password } = this.state
        return this.callApi( 
            () => signIn(email, password)
            .then( response => {
                if (response.ok) {
                    const { user, idToken } = response.json
                    const { onAuthChange, handleClose, onSuccess } = this.props
                    // clear the form
                    this.clearForm()
                    // set the app user in the state
                    onAuthChange(user, idToken)
                    // close the modal 
                    handleClose(e, true)
                    if (onSuccess) {
                        onSuccess(response)
                    }
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                } 
                return response
            })
        )
    }

    // invokes the api to sign up an user
    onSignUpClick = (e) => {
        const { displayName, email, password } = this.state        
        return this.callApi(
            () => signUp(displayName, email, password)
            .then( response => {
                (async () => {
                    if (response.ok) {
                        // sign in with the created user
                        const response = await signIn(email, password)
                        const { user, idToken } = response.json
                        const { onAuthChange, handleClose, onSuccess } = this.props
                        // clear the form
                        this.clearForm()
                        // set the app user in the state
                        onAuthChange(user, idToken)
                        // close the modal 
                        handleClose(e, true)
                        if (onSuccess) {
                            onSuccess(response)
                        }
                    } else {
                        // update error message
                        this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                    } 
                })();
                return response;
            } )
        )         
    } 

    // invokes the api to create an user
    onCreateClick = async (e) => {
        const { idToken } = this.props
        const { email, displayName, password, role} = this.state
        return this.callApi( 
            () => create(idToken, email, displayName, password, role) 
            .then( response => {
                if (response.ok) {
                    const { handleClose, onSuccess } = this.props
                    // clear the form
                    this.clearForm()
                    // close the modal 
                    handleClose(e, true)
                    if (onSuccess) {
                        onSuccess(response)
                    }
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                } 
                return response
            })
        )
    }
    
    // invokes the api to patch an user
    onPatchClick = async (e) => {
        const { idToken, targetUser } = this.props
        const { email, displayName, password, role} = this.state
        return this.callApi( 
            () => patch(idToken, targetUser.userId, email, displayName, password, role) 
            .then( response => {
                if (response.ok) {
                    const { handleClose, onSuccess } = this.props
                    // clear the form
                    this.clearForm()
                    // close the modal 
                    handleClose(e, true)
                    if (onSuccess) {
                        onSuccess(response)
                    }
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                } 
                return response
            })
        )
    }
    
    render () {
        const { mode, handleClose, user, targetUser } = this.props
        const { displayName, displayNameValid, email, emailValid, password, passwordValid, role, loading, errors } = this.state

        let showModal = true, formIsValid, title, showDisplayName = false, showRole = false, submitFunction, submitText, autocomplete
        switch(mode) {
            case SIGN_IN_MODE:
                title = 'Sign In'
                formIsValid = emailValid && passwordValid
                submitFunction = this.onSignInClick
                submitText = 'Sign In'
                autocomplete = 'current-password'
                break
            case SIGN_UP_MODE:
                title = 'Sign Up'
                formIsValid = displayNameValid && emailValid && passwordValid
                showDisplayName = true
                submitFunction = this.onSignUpClick
                submitText = 'Sign Up'
                autocomplete = 'new-password'
                break
            case CREATE_MODE:
                title = 'Create User'
                formIsValid = displayNameValid && emailValid && passwordValid
                showDisplayName = showRole = true
                submitFunction = this.onCreateClick
                submitText = 'Create'
                autocomplete = 'new-password'
                break
            case PATCH_MODE:
                title = 'Edit User'
                formIsValid = displayNameValid && emailValid && passwordValid
                showDisplayName = showRole = true
                submitFunction = this.onPatchClick
                submitText = 'Save'
                autocomplete = 'new-password'
                break
            default:
                showModal = false
        }

        return ( 
            <Modal backdrop={loading ? 'static' : true} show={showModal} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>{title}</Modal.Title>
                    {loading && <LoadingIcon/>}
                </Modal.Header>
                <Modal.Body>
                    {errors.map((error, index) => <Alert key={index} variant='danger'>{error.message ? error.message : error}</Alert>)}
                    <Form>
                        {showDisplayName &&                        
                            <Form.Group controlId="formBasicDisplayName">
                                <Form.Label>Display Name</Form.Label>
                                <ValidationIcon valid={displayNameValid} />
                                <Form.Control value={displayName} onChange={e => this.onControlChange(e, 'displayName')} disabled={loading}
                                    type="text" placeholder="Enter display name" />
                            </Form.Group>
                        }

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <ValidationIcon valid={emailValid} />
                            <Form.Control value={email} onChange={e => this.onControlChange(e, 'email')} disabled={loading} autoComplete="username"
                                type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <ValidationIcon valid={passwordValid} />
                            <Form.Control value={password} onChange={e => this.onControlChange(e, 'password')} disabled={loading} autoComplete={autocomplete}
                                type="password" placeholder="Password"/>
                        </Form.Group>  

                        {showRole && 
                            <Form.Group controlId="formBasicRole">
                                <Form.Label>Role</Form.Label>
                                <Form.Control value={role} onChange={e => this.onControlChange(e, 'role')} disabled={loading} as="select" custom >
                                    { [USER_ROLE, MANAGER_ROLE, ADMIN_ROLE].map(targetRole =>
                                        canCRUDrole(user, targetRole, targetUser) 
                                            && <option key={targetRole} value={targetRole} >{targetRole}</option>) }                                    
                                </Form.Control>
                            </Form.Group>
                        }
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleClose} disabled={loading} variant="secondary" >
                        Cancel
                    </Button>
                    <Button disabled={!formIsValid || loading} onClick={submitFunction} variant="primary" >
                        {submitText}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

