import React, { PureComponent }  from 'react'
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import { defaultListFilters } from '../lib/api.helper'
import { offsetUTC, splitOffset, dateToShortTimeString, canCRUDtimezone } from '../lib/timezones.helper'
import { all, remove } from '../services/timezones.service'
import { LoadingIcon } from './Loading'
import ResourcesTable from './ResourcesTable'
import UserSelector from './UserSelector'
import ConfirmationModal from './ConfirmationModal'
import EditTimezoneModal, { CREATE_MODE, PATCH_MODE } from './EditTimezoneModal'

/*
Timezones page component
*/
export default class Timezones extends PureComponent {
    constructor (props) {
        super(props) 
        const canCRUDallTimezones = canCRUDtimezone(this.props.user, null)
        this.state = {
            canCRUDallTimezones,
            date: new Date(),
            timezones: [],
            filters: {...defaultListFilters},
            modalMode: null,
            showDeleteModal: false,
            targetUser: (canCRUDallTimezones ? null : this.props.user),
            targetTimezone: null,
            loading: false,
            errors: []
        }
    }

    // creates an interval callback to update all clocks
    componentDidMount() {
        this.timerID = setInterval(
            () => this.updateDate(),
            1000
        );
    }
    // remove the clocks update interval callback
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    // update all clocks in the page
    updateDate = () => {
        this.setState({date : new Date()})
    }

    // dismisses an error alert
    onErrorClose = (e, index) => {
        // Remove the error clicked from the state        
        this.setState(state => {
            let newState = { errors : state.errors.filter((error, eIndex) => index !== eIndex) }
                return newState
            })
    }

    // invokes the api to get all timezones of a target user
    getAllTimezones = (filters) => {
        const { idToken } = this.props
        const targetUser = this.state.targetUser
        this.setState({ loading: true })

        all(idToken, targetUser.userId, filters)
            .then(response => {
                if (response.ok) {
                    this.setState({ timezones: response.json.timezones, filters: response.json.filters })
                } else {
                    // update the errors
                    this.setState(state => ({ errors: [...state.errors, response.json.message] }))
                }
                this.setState({ loading: false })
            })
            .catch(apiFunctionException => {
                // update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, apiFunctionException] }))
            })
    }

    // Opens the timezone modal in create mode
    onCreateTimezoneClick = (e) => {
        this.setState({ modalMode: CREATE_MODE })
    }

    // Opens the timezone modal in patch mode
    onPatchTimezoneClick = (e, targetTimezone) => {
        this.setState({ modalMode: PATCH_MODE, targetTimezone })
    }

    // Clears timezone modal data. triggers an 'get all' api call if needed
    handleEditModalClose = (e, success = false) => { 
        this.setState({ modalMode: null, targetTimezone: null })
        if (success) {
            this.getAllTimezones(this.state.filters)
        }
    }

    // Opens the timezone delete confirmation modal
    onDeleteTimezoneClick = (e, targetTimezone) => {
        this.setState({ showDeleteModal: true, targetTimezone })
    }

    // Clears timezone delete confirmation modal data.
    handleDeleteModalClose = (e) => { 
        this.setState({ showDeleteModal: false, targetTimezone: null  })
    }

    // invokes the api to delete a timezones of a target user
    handleDeleteModalConfirm = (e) => {
        // Get parameters for the delete
        const { idToken } = this.props
        const { targetUser, targetTimezone } = this.state
        // Close the modal, start the request
        this.setState({ showDeleteModal: false, targetTimezone: null, loading: true })
        // Call the api function
        remove(idToken, targetUser.userId, targetTimezone.timezoneId)
            .then(response => {
                if (response.ok) {
                    this.getAllTimezones(this.state.filters)
                } else {
                    // update the errors
                    this.setState(state => ({ loading: false, errors: [...state.errors, response.json.message] }))
                }
            })
            .catch(apiFunctionException => {
                // update the errors
                this.setState(state => ({ loading: false, errors: [...state.errors, apiFunctionException] }))
            })
    }

    // saves selected target user to the state
    onSelectUserClick = (e, targetUser) => {
        this.setState({ timezones: [], targetUser })
    }

    render () {
        const { canCRUDallTimezones, date, timezones, modalMode, targetUser, targetTimezone, showDeleteModal, loading, errors, filters } = this.state

        const time = date.getTime()
        const myTimezoneOffset = date.getTimezoneOffset()

        // current time in that timezone and the difference between the browser’s time.
        const filteredTimezones = timezones.map(timezone => {
                const offsetDifference = timezone.offset - myTimezoneOffset
                const offsetDiffObj = splitOffset(offsetDifference)
                const offsetDiffStr = 
                    (offsetDiffObj.offsetHours || offsetDiffObj.offsetMinutes 
                        ?   (offsetDiffObj.sign === -1 ? '+ ' : '- ') 
                            + (offsetDiffObj.offsetHours ? `${offsetDiffObj.offsetHours} hour${offsetDiffObj.offsetHours > 1 ? 's' : ''} ` : '' )
                            + (offsetDiffObj.offsetMinutes ? `${offsetDiffObj.offsetMinutes} minute${offsetDiffObj.offsetMinutes > 1 ? 's' : ''}` : '' )
                        : '0')
                const timezoneTime = new Date(time - offsetDifference * 60 * 1000)
                const timezoneTimeStr = dateToShortTimeString(timezoneTime)
                
                return {...timezone, utc: offsetUTC(timezone.offset) /* replaces offset integer with a formated string! */, difference: offsetDiffStr, timezoneTime: timezoneTimeStr}
            })
        const timezonesColumns = [ 
            { title: 'Name', key: 'name', sortable: true } , 
            { title: 'City', key: 'city', sortable: true } , 
            { title: 'UTC', key: 'utc', sortable: true, sortKey: 'offset' } , 
            { title: 'Difference', key: 'difference' },
            { title: 'Time', key: 'timezoneTime' }
        ]

        return <Container>
            <h1>Timezones { loading && <LoadingIcon /> }</h1>
            <h6>Current time: {date.toTimeString()}</h6>
            
            {errors.map((error, index) => 
                <Alert key={index} onClose={e => this.onErrorClose(e, index)} variant='danger' dismissible >{error.message ? error.message : error}</Alert>)}
            
            {canCRUDallTimezones && <UserSelector {...this.props} targetUser={targetUser} onSelectUserClick={this.onSelectUserClick}></UserSelector>}

            {targetUser && <ResourcesTable resourceName='timezone' resources={filteredTimezones} filters={filters}
                columns={timezonesColumns} keyName={'timezoneId'} 
                loading={loading}
                onCreateResourceClick={this.onCreateTimezoneClick} onRefresh={this.getAllTimezones}
                onPatchResourceClick={this.onPatchTimezoneClick} onDeleteResourceClick={this.onDeleteTimezoneClick}
                ></ResourcesTable>}

            {modalMode && <EditTimezoneModal {...this.props} mode={modalMode} targetUser={targetUser} targetTimezone={targetTimezone} handleClose={this.handleEditModalClose} ></EditTimezoneModal>}
            {showDeleteModal && 
                <ConfirmationModal show={true} title='Delete timezone' message={`Are you sure you want to delete the timezone ${targetTimezone.name}, ${targetTimezone.city}`} 
                    confirmText='Delete timezone' handleClose={this.handleDeleteModalClose} handleConfirm={this.handleDeleteModalConfirm} ></ConfirmationModal>}
        </Container>
    }
}