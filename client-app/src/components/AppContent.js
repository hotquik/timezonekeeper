import React , { PureComponent } from 'react'
import { Switch, Route, Redirect } from "react-router-dom"
import { routes } from '../lib/routes.helper'
import Home from './Home.js'
import Users from './Users.js'
import Timezones from './Timezones.js'

/*
 A wrapper for <Route> that redirects to the login screen if you're not yet authenticated.
*/
function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        rest.user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: routes.home.path,
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

/*
Main router component
*/
export default class AppContent extends PureComponent {
    render () {
        const { timezones, users, home } = routes

        return (
            <main className="p-3">
                <Switch>
                    <PrivateRoute path={timezones.path} {...this.props}>
                      <Timezones {...this.props}/>
                    </PrivateRoute>

                    <PrivateRoute path={users.path} {...this.props}>
                      <Users {...this.props}/>
                    </PrivateRoute>

                    <Route path={home.path} {...this.props}>
                      <Home {...this.props}/>
                    </Route>
                </Switch>
            </main>
        )
    }
}

