import { routes } from './routes.helper'

export const USER_ROLE = 'user'
export const MANAGER_ROLE = 'manager'
export const ADMIN_ROLE = 'admin'

const { timezones, users, profile } = routes

const role_routes_permissions = [
    { role: USER_ROLE, route: profile, permission: true },
    { role: USER_ROLE, route: timezones, permission: true },

    { role: MANAGER_ROLE, route: profile, permission: true },
    { role: MANAGER_ROLE, route: timezones, permission: false },
    { role: MANAGER_ROLE, route: users, permission: true },

    { role: ADMIN_ROLE, route: profile, permission: true },
    { role: ADMIN_ROLE, route: timezones, permission: true },
    { role: ADMIN_ROLE, route: users, permission: true },
]

export function roleCanAccessRoute (role, route) {
    const permissionRow = role_routes_permissions.find(row => row.role === role && row.route === route)
    // if no row found in the table, return false
    if (!permissionRow) {
        return false
    }
    return permissionRow.permission

}

/*
Logic taken from users/controller.ts in functions project
*/

const ROLE_PERMISSION_NONE = 'none'
const ROLE_PERMISSION_SELF = 'self'
const ROLE_PERMISSION_ALL = 'all'

const role_crud_permissions = [
    { authenticatedUserRole: USER_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_SELF}, 
    { authenticatedUserRole: USER_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: USER_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: MANAGER_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_NONE }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: USER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: MANAGER_ROLE, permission: ROLE_PERMISSION_ALL }, 
    { authenticatedUserRole: ADMIN_ROLE, targetUserRole: ADMIN_ROLE, permission: ROLE_PERMISSION_ALL }, 
]

export function canCRUDrole (authenticatedUser, targetRole, appUser) {
    if (!authenticatedUser) {
        return false
    }
    const authenticatedUserRole = authenticatedUser.role
    // Find the specific permission row in the table
    const permissionRow = role_crud_permissions.find(item => item.authenticatedUserRole === authenticatedUserRole && item.targetUserRole === targetRole)
    if (!permissionRow) {
        // If no row was found 
        return false
    } else {        
        if (permissionRow.permission === ROLE_PERMISSION_NONE) {
            // If permission is set to none, return false
            return false
        } else if (permissionRow.permission === ROLE_PERMISSION_ALL) {
            // If permission is set to all, return true       
            return true
        } else if (permissionRow.permission === ROLE_PERMISSION_SELF) {
            const authenticatedUserUid = authenticatedUser.userId
            // If permission is set to self, check that the target user and the authenticated user are the same    
            return (authenticatedUserUid && appUser && appUser.userId && authenticatedUserUid === appUser.userId)
        } else {
            // No true condition matched. return false as default
            return false
        }
    }
}