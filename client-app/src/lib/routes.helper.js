/*
App routes
*/
export const routes = {
    timezones: {
        path: "/timezones",
        title: "Timezones"
    },
    users: {
        path: "/users",
        title: "Users"
    }, 
    home: {
        path: "/",
        title: "Home"
    }
  }
