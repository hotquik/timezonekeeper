import { USER_ROLE, MANAGER_ROLE, ADMIN_ROLE } from '../lib/users.helper'

const TIMEZONE_PERMISSION_NONE = 'none'
const TIMEZONE_PERMISSION_SELF = 'self'
const TIMEZONE_PERMISSION_ALL = 'all'

const timezone_crud_permissions = [
    { authenticatedUserRole: USER_ROLE, permission: TIMEZONE_PERMISSION_SELF}, 
    { authenticatedUserRole: MANAGER_ROLE, permission: TIMEZONE_PERMISSION_NONE },
    { authenticatedUserRole: ADMIN_ROLE, permission: TIMEZONE_PERMISSION_ALL }
]

export function canCRUDtimezone (authenticatedUser, targetUser) {
    if (!authenticatedUser) {
        return false
    }
    const authenticatedUserRole = authenticatedUser.role
    // Find the specific permission row in the table
    const permissionRow = timezone_crud_permissions.find(item => item.authenticatedUserRole === authenticatedUserRole)
    if (!permissionRow) {
        // If no row was found 
        return false
    } else {        
        if (permissionRow.permission === TIMEZONE_PERMISSION_NONE) {
            // If permission is set to none, return false
            return false
        } else if (permissionRow.permission === TIMEZONE_PERMISSION_ALL) {
            // If permission is set to all, return true       
            return true
        } else if (permissionRow.permission === TIMEZONE_PERMISSION_SELF) {
            const authenticatedUserUid = authenticatedUser.userId
            // If permission is set to self, check that the target user and the authenticated user are the same    
            return (authenticatedUserUid && targetUser && targetUser.userId && authenticatedUserUid === targetUser.userId)
        } else {
            // No true condition matched. return false as default
            return false
        }
    }
}


/*
* Helper functions
*/
function padStart(timepart) {
    return ("0" + timepart).slice(-2)
}

export function dateToShortTimeString (date) {
    return `${padStart(date.getHours())}:${padStart(date.getMinutes())}:${padStart(date.getSeconds())}`
}

export function splitOffset (offset) {
    const sign = (offset > 0 ? 1 : -1)
    const absUtc = Math.abs(offset)
    const offsetHours = Math.floor(absUtc / 60)
    const offsetMinutes = absUtc % 60  
    return { sign, offsetHours, offsetMinutes }
}

export function joinOffset ({ sign, offsetHours , offsetMinutes }) {
    return sign * ( (offsetHours * 60) + offsetMinutes )
}

export function offsetUTC (offset) {
    const offsetObj = splitOffset(offset)
    let sign = (offsetObj.sign === 1 ? '-' : '+')
    return `UTC${sign}${padStart(offsetObj.offsetHours)}:${padStart(offsetObj.offsetMinutes)}`
}

export const timezonesOffsets = [
    { offset:720, name:'UTC−12:00, Y', city:'United States Minor Outlying Islands' },
    { offset:660, name:'UTC−11:00, X', city:'American Samoa' },
    { offset:600, name:'UTC−10:00, W', city:'Honolulu' },
    { offset:570, name:'UTC−09:30, V†', city:'French Polynesia' },
    { offset:540, name:'UTC−09:00, V', city:'Anchorage' },
    { offset:480, name:'UTC−08:00, U', city:'Los Angeles, Vancouver, Tijuana' },
    { offset:420, name:'UTC−07:00, T', city:'Phoenix, Calgary, Ciudad Juárez' },
    { offset:360, name:'UTC−06:00, S', city:'Mexico City, Chicago, Guatemala City, Tegucigalpa, Winnipeg, San José, San Salvador' },
    { offset:300, name:'UTC−05:00, R', city:'New York, Toronto, Havana, Lima, Bogotá, Kingston' },
    { offset:240, name:'UTC−04:00, Q', city:'Santiago, Santo Domingo, Manaus, Caracas, La Paz, Halifax' },
    { offset:210, name:'UTC−03:30, P†', city:'St. John’s' },
    { offset:180, name:'UTC−03:00, P', city:'São Paulo, Buenos Aires, Montevideo' },
    { offset:120, name:'UTC−02:00, O', city:'South Georgia and the South Sandwich Islands' },
    { offset:60, name:'UTC−01:00, N', city:'Greenland' },
    { offset:0, name:'UTC±00:00, Z', city:'London, Dublin, Lisbon, Abidjan, Accra, Dakar' },
    { offset:-60, name:'UTC+01:00, A', city:'Berlin, Rome, Paris, Madrid, Warsaw, Lagos, Kinshasa, Algiers, Casablanca' },
    { offset:-120, name:'UTC+02:00, B', city:'Cairo, Johannesburg, Khartoum, Kiev, Bucharest, Athens, Jerusalem, Sofia' },
    { offset:-180, name:'UTC+03:00, C', city:'Moscow, Istanbul, Riyadh, Baghdad, Addis Ababa, Doha' },
    { offset:-210, name:'UTC+03:30, C†', city:'Tehran' },
    { offset:-240, name:'UTC+04:00, D', city:'Dubai, Baku, Tbilisi, Yerevan, Samara' },
    { offset:-270, name:'UTC+04:30, D†', city:'Kabul' },
    { offset:-300, name:'UTC+05:00, E', city:'Karachi, Tashkent, Yekaterinburg' },
    { offset:-330, name:'UTC+05:30, E†', city:'Mumbai, Colombo' },
    { offset:-345, name:'UTC+05:45, E*', city:'Kathmandu' },
    { offset:-360, name:'UTC+06:00, F', city:'Dhaka, Almaty, Omsk' },
    { offset:-390, name:'UTC+06:30, F†', city:'Yangon' },
    { offset:-420, name:'UTC+07:00, G', city:'Jakarta, Ho Chi Minh City, Bangkok, Krasnoyarsk' },
    { offset:-480, name:'UTC+08:00, H', city:'Shanghai, Taipei, Kuala Lumpur, Singapore, Perth, Manila, Makassar, Irkutsk' },
    { offset:-525, name:'UTC+08:45, H*', city:'Eucla' },
    { offset:-540, name:'UTC+09:00, I', city:'Tokyo, Seoul, Pyongyang, Ambon, Yakutsk' },
    { offset:-570, name:'UTC+09:30, I†', city:'Adelaide' },
    { offset:-600, name:'UTC+10:00, K', city:'Sydney, Port Moresby, Vladivostok' },
    { offset:-630, name:'UTC+10:30, K†', city:'Lord Howe Island' },
    { offset:-660, name:'UTC+11:00, L', city:'Norfolk Island' },
    { offset:-720, name:'UTC+12:00, M', city:'Auckland, Suva, Petropavlovsk-Kamchatsky' },
    { offset:-765, name:'UTC+12:45, M*', city:'Chatham Islands' },
    { offset:-780, name:'UTC+13:00, M†', city:'Phoenix Islands' },
    { offset:-840, name:'UTC+14:00, M†', city:'Line Islands' }
]