/*
Helper functions and const for api calls
*/
export const API_URL = process.env.REACT_APP_API_URL 

if (!API_URL) {
  console.log("Error: 'REACT_APP_API_URL' is undefined!")
}

export const _requestContentType = async (
  url,
  method = 'GET',
  contentType = 'application/json',
  token = '',
  parameters = {}, 
  body = {}
) => {
  let headers = {
    'Content-Type': contentType
  }

  if (token) headers['Authorization'] = `Bearer ${token}`

  let options = {
    method,
    headers: headers
  }

  // Request with GET/HEAD method cannot have body.
  if (method !== 'GET' && method !== 'HEAD') {
    options['body'] = JSON.stringify(body)
  }

  const requestUrl = new URL(url);
  Object.entries(parameters).forEach( ([key, value]) => requestUrl.searchParams.append(key, value))
  
  return fetch(requestUrl, options)
}

const _requestJSON = async (url, method = 'GET', token = '', parameters = {}, body = {}) => {
  const response = await _requestContentType(
    url,
    method,
    'application/json',
    token,
    parameters,
    body
  )

  let json = undefined
  if (method !== 'DELETE') {
    json = await response.json()
  }

  return {
    ok: response.ok,
    redirected: response.redirected,
    headers: response.headers,
    status: response.status,
    json
  }
}

export const _getJSON = async (url, token, parameters) =>
  _requestJSON(url, 'GET', token, parameters)

export const _postJSON = async (url, token, parameters, body) =>
  _requestJSON(url, 'POST', token, parameters, body)

export const _putJSON = async (url, token, parameters, body) =>
  _requestJSON(url, 'PUT', token, parameters, body)

export const _patchJSON = async (url, token, parameters, body) =>
  _requestJSON(url, 'PATCH', token, parameters, body)

export const _delete = async (url, token, parameters) =>
  _requestJSON(url, 'DELETE', token, parameters)

export const defaultPageSizes = [5, 10, 20, 50]

export const defaultListFilters = {
  searchText: '',
  sortField: '', 
  sortOrder: '', 
  pageSize: defaultPageSizes[0],
  pageNumber: 1,
  pagesCount: 1
}